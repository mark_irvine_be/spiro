import svgwrite
import math, random
from fractions import gcd

dwg = svgwrite.Drawing(filename='spiro2c.svg')
dwg.viewbox(width=1000, height=1000)


def rcol():
    return "rgb("+str(random.randint(0, 255))+","+str(random.randint(0, 255))+","+str(random.randint(0, 255))+")"

def rcols():
    cols = []
    """
    baser = random.randint(0, 200)
    baseg = random.randint(10, 180)
    baseb = random.randint(10, 200)
    """
    baser = 200
    baseg = 180
    baseb = random.randint(10, 200)

    for n in range(5):
        c = "rgb(" + str(baser+(n*10)) + "," + str(baseg+(n*10)) + ","+ str(baseb+(n*10)) + ")"
        cols.append(c)

    return cols

scale = 400
size  = 10
d = 17
for row in range(size):
    for col in range(size):

        a =  random.randint(50,100)
        b = random.randint(25,50)
        d = random.randint(20,40)
        bstep = random.randint(1,10)
        n = random.randint(3,5)
        n=1
        ang = random.randint(3,5)
        """
        a =  80
        b = 55
        d = d=d+1
        bstep = 5
        n = 1
        ang = 3
        """
        colors = [rcol(), rcol(), rcol(), rcol(), rcol()]
        #colors = rcols()
        colors =["rgb(0,0,100)"]
        print(colors)



        def create_spiro(a, b, d):
            dt = 0.01
            t = 0;
            pts = []
            while t < 2*math.pi*b/gcd(a, b):
                t += dt
                x = (a - b) * math.cos(t) + d * math.cos((a - b)/b * t)
                y = (a - b) * math.sin(t) - d * math.sin((a - b)/b * t)
                pts.append((x, y))
            return pts

        group = svgwrite.container.Group(transform='translate('+str(scale*row)+','+str(scale*col)+')')

        dwg.add(group)
        for i in range(n):
            pts = create_spiro(a, b-bstep*i, d)
            rot = 'rotate({})'.format(i*ang)
            polygon = dwg.polygon(points=pts, stroke=colors[i], stroke_width=0.1,
                                    fill='none', transform=rot)
            group.add(polygon)
            t = dwg.text("a: %s b: %s d: %s bstep: %s ang %s" %(a,b,d,bstep, ang),
                         insert=(-200, 200),
                         fill="rgb(0,0,200)",
                         style="font-size:10px; font-family:Arial")
            group.add(t)
        print("done: ",row,col,a,b,d,bstep,n,ang)
dwg.save()

"""
        dwg.add(dwg.rect(insert = (0, 0),
                             size = ("2000px", "2000px"),
                             stroke_width = "1",
                             stroke = "green",
                             fill = "rgb(0,0,0)"))
"""