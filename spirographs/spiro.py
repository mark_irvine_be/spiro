import svgwrite
import math
from fractions import gcd

a = 220
b = 96
d = 2

def create_spiro(a, b, d):
    dt = 0.01
    t = 0;
    pts = []
    while t < 2*math.pi*b/gcd(a, b):
        t += dt
        x = (a - b) * math.cos(t) + d * math.cos((a - b)/b * t)
        y = (a - b) * math.sin(t) - d * math.sin((a - b)/b * t)
        pts.append((x, y))
    return pts

dwg = svgwrite.Drawing(filename='spiro.svg')
dwg.viewbox(width=1000, height=1000)

pts = create_spiro(a, b, d)
polygon = dwg.polygon(points=pts, stroke='red', stroke_width=10,
                        fill='none', transform='translate(500, 500)')
dwg.add(polygon)

dwg.save()
