import svgwrite
import math, random
from fractions import gcd

dwg = svgwrite.Drawing(filename='cards.svg')
dwg.viewbox(width=500, height=400)

group = svgwrite.container.Group()
dwg.add(group)

def make_card():

    card = dwg.rect(size  = (95,146),
                 ry=3.5,
                 insert=(0, -146),
                 fill="rgb(0,0,0)")
    group.add(card)

    footer = dwg.rect(size  = (85,12),
                 ry=1.5,
                 insert=(5, -17),
                 fill="rgb(255,255,255)")
    group.add(footer)

    mainbox = dwg.rect(size  = (85,85),
                 ry=1.5,
                 insert=(5, -107),
                 fill="rgb(255,255,255)")
    group.add(mainbox)

    minibox = dwg.rect(size  = (20,12),
                 ry=1.5,
                 insert=(5, -124),
                 fill="rgb(255,255,255)")
    group.add(minibox)

    smallbox = dwg.rect(size  = (60,12),
                 ry=1.5,
                 insert=(30, -124),
                 fill="rgb(255,255,255)")
    group.add(smallbox)

    header = dwg.rect(size  = (85,12),
                 ry=1.5,
                 insert=(5, -141),
                 fill="rgb(255,255,255)")
    group.add(header)


make_card()
dwg.save()
