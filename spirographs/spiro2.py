import svgwrite
import math
from fractions import gcd

a = 500
b = 275
d = 225
dstep = 20
n = 10

def create_spiro(a, b, d):
    dt = 0.01
    t = 0;
    pts = []
    while t < 2*math.pi*b/gcd(a, b):
        t += dt
        x = (a - b) * math.cos(t) + d * math.cos((a - b)/b * t)
        y = (a - b) * math.sin(t) - d * math.sin((a - b)/b * t)
        pts.append((x, y))
    return pts

dwg = svgwrite.Drawing(filename='spiro2.svg')
dwg.viewbox(width=1000, height=1000)

group = svgwrite.container.Group(transform='translate(500, 500)')
dwg.add(group)

for i in range(n):
    pts = create_spiro(a, b, d-dstep*i)
    polygon = dwg.polygon(points=pts, stroke='blue', stroke_width=2,
                            fill='none')
    group.add(polygon)

dwg.save()
