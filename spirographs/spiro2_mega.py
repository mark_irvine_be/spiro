import svgwrite
import math, random
from fractions import gcd

dwg = svgwrite.Drawing(filename='spiro2-mega.svg')
dwg.viewbox(width=1000, height=1000)


def rcol():
    return "rgb("+str(random.randint(0, 255))+","+str(random.randint(0, 255))+","+str(random.randint(0, 255))+")"

def rcols(count):
    cols = []
    baser = random.randint(0, 200)
    baseg = random.randint(10, 30)
    baseb = random.randint(1, 20)

    for n in range(count):
        c = "rgb(" + str(baser+(n*10)) + "," + str(baseg+(n*25)) + ","+ str(baseb+(n*40)) + ")"
        cols.append(c)

    return cols

a =  random.randint(100,200)
b = random.randint(250,400)
d = random.randint(500,600)
bstep = random.randint(5,50)
n = random.randint(5,10)
ang = random.randint(3,5)
#colors = [rcol(), rcol(), rcol(), rcol(), rcol()]
colors = rcols(n)
print(colors)



def create_spiro(a, b, d):
    dt = 0.01
    t = 0;
    pts = []
    while t < 2*math.pi*b/gcd(a, b):
        t += dt
        x = (a - b) * math.cos(t) + d * math.cos((a - b)/b * t)
        y = (a - b) * math.sin(t) - d * math.sin((a - b)/b * t)
        pts.append((x, y))
    return pts

group = svgwrite.container.Group(transform='translate(400,400)')

dwg.add(group)
for i in range(n):
    pts = create_spiro(a, b-bstep*i, d)
    rot = 'rotate({})'.format(i*ang)
    polygon = dwg.polygon(points=pts, stroke=colors[i], stroke_width=0.1,
                            fill='none', transform=rot)
    group.add(polygon)
    t = dwg.text("a: %s b: %s d: %s bstep: %s ang %s" %(a,b,d,bstep, ang),
                 insert=(-400, 400),
                 fill="rgb(0,0,200)",
                 style="font-size:10px; font-family:Arial")
    group.add(t)
print("done: ",a,b,d,bstep,n,ang)
dwg.save()

