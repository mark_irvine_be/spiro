import svgwrite
import math, random
from fractions import gcd

dwg = svgwrite.Drawing(filename='cards.svg')
dwg.viewbox(width=500, height=400)

def rcol():
    return "rgb("+str(random.randint(0, 255))+","+str(random.randint(0, 255))+","+str(random.randint(0, 255))+")"

def rcols():
    cols = []
    """
    baser = random.randint(0, 200)
    baseg = random.randint(10, 180)
    baseb = random.randint(10, 200)
    """
    baser = 200
    baseg = 180
    baseb = random.randint(10, 200)

    for n in range(5):
        c = "rgb(" + str(baser+(n*10)) + "," + str(baseg+(n*10)) + ","+ str(baseb+(n*10)) + ")"
        cols.append(c)

    return cols

group_card  = svgwrite.container.Group()
group_spiro = svgwrite.container.Group(transform='translate(47.5,-64.5)')


dwg.add(group_card)
dwg.add(group_spiro)

def make_card():

    card = dwg.rect(size  = (95,146),
                 ry=3.5,
                 insert=(0, -146),
                 fill="rgb(0,0,0)")
    group_card.add(card)

    footer = dwg.rect(size  = (85,12),
                 ry=1.5,
                 insert=(5, -17),
                 fill="rgb(255,255,255)")
    group_card.add(footer)

    mainbox = dwg.rect(size  = (85,85),
                 ry=1.5,
                 insert=(5, -107),
                 fill="rgb(255,255,255)")
    group_card.add(mainbox)

    minibox = dwg.rect(size  = (20,12),
                 ry=1.5,
                 insert=(5, -124),
                 fill="rgb(255,255,255)")
    group_card.add(minibox)

    smallbox = dwg.rect(size  = (60,12),
                 ry=1.5,
                 insert=(30, -124),
                 fill="rgb(255,255,255)")
    group_card.add(smallbox)

    header = dwg.rect(size  = (85,12),
                 ry=1.5,
                 insert=(5, -141),
                 fill="rgb(255,255,255)")
    group_card.add(header)


def make_spiro(a,b,c):
    colors = [rcol(), rcol(), rcol(), rcol(), rcol()]
    print(colors)
    dt = 0.01
    t = 0;
    pts = []
    while t < 2 * math.pi * b / gcd(a, b):
        t += dt
        x = (a - b) * math.cos(t) + d * math.cos((a - b) / b * t)
        y = (a - b) * math.sin(t) - d * math.sin((a - b) / b * t)
        pts.append((x, y))
    return pts


make_card()

a = random.randint(10, 50)
b = random.randint(5, 25)
d = random.randint(10, 20)
bstep = random.randint(1, 5)
n = random.randint(2, 4)
ang = random.randint(3, 5)
colors = [rcol(), rcol(), rcol(), rcol(), rcol()]
print(colors)
for i in range(n):
    pts = make_spiro(a, b-bstep*i, d)
    rot = 'rotate({})'.format(i*ang)
    polygon = dwg.polygon(points=pts,
                          stroke=colors[i],
                          stroke_width=0.5,
                          fill='none', transform=rot)
    group_spiro.add(polygon)


t = dwg.text("a: %s b: %s d: %s bstep: %s angle: %s" %(a,b,d,bstep, ang),
             insert=(7,-10),
             fill="rgb(0,0,200)",
             style="font-size:5px; font-family:Arial")
group_card.add(t)
print("done: ",a,b,d,bstep,n,ang)


dwg.save()
