import svgwrite
import math
from fractions import gcd

a = 500
b = 350
d = 200
dstep = 10
n = 5
ang = 5

def create_spiro(a, b, d):
    dt = 0.01
    t = 0;
    pts = []
    while t < 2*math.pi*b/gcd(a, b):
        t += dt
        x = (a - b) * math.cos(t) + d * math.cos((a - b)/b * t)
        y = (a - b) * math.sin(t) - d * math.sin((a - b)/b * t)
        pts.append((x, y))
    return pts

dwg = svgwrite.Drawing(filename='spiro2a.svg')
dwg.viewbox(width=1000, height=1000)

group = svgwrite.container.Group(transform='translate(500, 500)')
dwg.add(group)

for i in range(n):
    pts = create_spiro(a, b, d-dstep*i)
    rot = 'rotate({})'.format(i*ang)
    polygon = dwg.polygon(points=pts, stroke='darkorange', stroke_width=2,
                            fill='none', transform=rot)
    group.add(polygon)

dwg.save()
