import svgwrite

#Definitions
filename = 'barchartside.svg'
WIDTH = 1300
HEIGHT = 800
ORIGINX = 50
ORIGINY = HEIGHT-50
UNITX = 100
UNITY = 30

monthlyHighTemp = [7.0, 7.4, 10.3, 13.1, 16.6, 19.6,
               22.1, 21.9, 18.7, 14.4, 10.0, 7.2]
monthlyLowTemp = [0.8, 0.6, 2.3, 3.5, 6.3, 9.2,
               11.5, 11.5, 9.5, 6.6, 3.3, 1.7]

def drawAxes():
    '''Draw the x and y axes for the graph'''
    line = dwg.line(start=(ORIGINX, ORIGINY),
             end=(WIDTH, ORIGINY),
             stroke='black',
             stroke_width = 5)
    dwg.add(line)
    line = dwg.line(start=(ORIGINX, ORIGINY),
             end=(ORIGINX, 0),
             stroke='black',
             stroke_width = 5)
    dwg.add(line)

def drawBars(data, color, index):
    for month in range(12):
        barHeight = data[month]*UNITY;
        barPosition = ORIGINX + (month+index/2)*UNITX;
        bar = dwg.rect(insert=(barPosition, ORIGINY - barHeight),
               size=(UNITX/2, barHeight), fill=color,
                    stroke='black', stroke_width=5)
        dwg.add(bar)


# Open the drawing
dwg = svgwrite.Drawing(filename)
dwg.viewbox(width=WIDTH, height=HEIGHT)


#Drawing code
drawBars(monthlyHighTemp, 'orange', 1)
drawBars(monthlyLowTemp, 'blue', 0)
drawAxes()

#Save the file
dwg.save()
