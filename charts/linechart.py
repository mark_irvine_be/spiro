import svgwrite

#Definitions
filename = 'linechart.svg'
WIDTH = 1300
HEIGHT = 800
ORIGINX = 50
ORIGINY = HEIGHT-50
UNITX = 100
UNITY = 30

monthlyTemp = [7.0, 7.4, 10.3, 13.1, 16.6, 19.6,
               22.1, 21.9, 18.7, 14.4, 10.0, 7.2]

def drawAxes():
    '''Draw the x and y axes for the graph'''
    line = dwg.line(start=(ORIGINX, ORIGINY),
             end=(WIDTH, ORIGINY),
             stroke='black',
             stroke_width = 5)
    dwg.add(line)
    line = dwg.line(start=(ORIGINX, ORIGINY),
             end=(ORIGINX, 0),
             stroke='black',
             stroke_width = 5)
    dwg.add(line)

def drawLines():
    points = []
    for month in range(12):
        y = ORIGINY - monthlyTemp[month]*UNITY;
        x = ORIGINX + month*UNITX + UNITX/2;
        point = (x, y)
        circle = dwg.circle(center=point, r=10,
                    fill='orange',)
        dwg.add(circle)
        points.append(point)
    line = dwg.polyline(points, stroke='orange',
                       stroke_width=5, fill='none')
    dwg.add(line)


# Open the drawing
dwg = svgwrite.Drawing(filename)
dwg.viewbox(width=WIDTH, height=HEIGHT)


#Drawing code
drawLines()
drawAxes()

#Save the file
dwg.save()
