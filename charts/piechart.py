import svgwrite
import math

#Definitions
filename = 'piechart.svg'
WIDTH = 1000
HEIGHT = 1000
X = 500
Y = 500
RADIUS = 400

values = [60, 20, 10, 6, 4]
colors = ["red", "green", "blue", "orange", "purple"]

def makeSegment(center=(0, 0), r = 1, start=0, end=360, **extra):
    '''
    makeSegment - draw a segment of a circle

    center - (x, y) position of segment center
    r - radius of segment
    atart - start angle in degrees
    end - end angle in degrees
    extra - fill, stroke etc parameters
    '''
    startRad = start*math.pi/180.0
    endRad = end*math.pi/180.0
    x1 = center[0] + r*math.cos(startRad)
    y1 = center[1] + r*math.sin(startRad)
    x2 = center[0] + r*math.cos(endRad)
    y2 = center[1] + r*math.sin(endRad)
    diff = (end-start)%360;
    largeArc = 0 if diff < 180 else 1
    pathStr = 'M {} {} '.format(center[0], center[1]);
    pathStr += 'L {} {} '.format(x1, y1);
    pathStr += 'A {} {} 0 {} {} {} {}'.format(r, r, largeArc, 1, x2, y2);
    pathStr += 'Z';
    return dwg.path(d=pathStr, **extra);

def drawPie():
    start = 0
    for i, x in enumerate(values):
        angle = x*360/100
        seg = makeSegment(center=(X, Y),
                          r=RADIUS, start=start, end=start+angle,
                          fill=colors[i], stroke='black',
                          stroke_width=5, stroke_linejoin='bevel')
        dwg.add(seg)
        start += angle


# Open the drawing
dwg = svgwrite.Drawing(filename)
dwg.viewbox(width=WIDTH, height=HEIGHT)


#Drawing code
drawPie()

#Save the file
dwg.save()
