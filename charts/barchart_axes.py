import svgwrite

WIDTH = 1300
HEIGHT = 800
ORIGIN_X = 50
ORIGIN_Y = HEIGHT-50

def drawAxes(dwg):
    '''Draw the x and y axes for the graph'''
    line = dwg.line(start=(ORIGIN_X, ORIGIN_Y),
             end=(WIDTH, ORIGIN_Y),
             stroke='black',
             stroke_width = 5)
    dwg.add(line)
    line = dwg.line(start=(ORIGIN_X, ORIGIN_Y),
             end=(ORIGIN_X, 0),
             stroke='black',
             stroke_width = 5)
    dwg.add(line)

#Draw the graph
dwg = svgwrite.Drawing('barchart.svg')
dwg.viewbox(width=WIDTH, height=HEIGHT)
drawAxes(dwg)
dwg.save()
